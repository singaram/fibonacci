import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class UnitTest {

	@Test
	public void testFibonacciSet()
	{
		//FibonacciSet set = new FibonacciSet();
		long[] inputList = {1, 1, 2, 3, 5}; 
		long[] resultList = FibonacciSet.iterative(5);
		assertArrayEquals(inputList, resultList);
	}
	
	@Test
	public void testFibonacciForCeiling()
	{
		List<Long> inputList = new ArrayList<Long>();
		inputList.add((long) 1);
		inputList.add((long) 1);
		inputList.add((long) 2);
		inputList.add((long) 3);
		inputList.add((long) 5);
		List<Long> resultList = FibonacciForCeiling.fibonacciSet(6);
		assertEquals(inputList, resultList);
	}
	
	@Test
	public void testNextFibonacciNumber()
	{
		List<Long> inputList = new ArrayList<Long>();
		inputList.add((long) 1);
		inputList.add((long) 1);
		inputList.add((long) 2);
		inputList.add((long) 3);
		inputList.add((long) 5);
		long checkValue = 8;
		long result = NextFibonacciNumber.nextFibo(inputList);
		assertEquals(checkValue, result);
	}
	
	@Test
	public void testValidateFiboSequence()
	{
		List<Long> inputList = new ArrayList<Long>();
		inputList.add((long) 1);
		inputList.add((long) 1);
		inputList.add((long) 2);
		inputList.add((long) 3);
		inputList.add((long) 5);
		long[] fiboList = new long[inputList.size()];
		boolean result = NextFibonacciNumber.validateFiboSequence(inputList, fiboList);
		assertEquals(true, result);
	}
}
