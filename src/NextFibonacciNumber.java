import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Get the next fibonacci number for a given fibonacci sequence
 * @author Singaram
 */
public class NextFibonacciNumber {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws NumberFormatException, IOException 
	{	
		// max limit that can execute in less than a second
		int maxLimit = 170000000;
		long nextFiboNumber = 0;
		boolean invalidInput = false;

		// get the fibonacci sequence from user
		System.out.println("Enter the fibonacci sequence (enter 'end' to stop input)");
		Scanner in = new Scanner(System.in);
		List<Long> list = new ArrayList<Long>();

		// get user input until 'end' is entered
		try
		{
			while (true)
			{
				String s = in.next();
				if (s.equals("end"))
					break;
				else
					// add each fibonacci number to the list
					list.add(Long.parseLong(s));
			}
		}
		catch (Exception e)
		{
			invalidInput = true;
			System.out.println("Invalid input");
		}

		// if valid input is entered, do the following
		if(!invalidInput)
		{
			if (list.size() > maxLimit)
			{
				// print an error on console as limit exceeded maximum
				System.out.println("Unable to calculate fibonacci sequence as size greater than max limit is entered");
				System.out.println("Max limit is 170000000");
			}
			else
			{
				// keep track of starting time
				long startTime = System.currentTimeMillis();

				// create a list for input sequence size
				long[] fiboList = new long[list.size()];

				// validate entered input sequence
				boolean validSequence = validateFiboSequence(list, fiboList);

				// call nextFibo to get the next fibonacci number 
				if(validSequence)
				{
					nextFiboNumber = nextFibo(list);
				}

				// track the end time and calculate the execution time
				long stopTime = System.currentTimeMillis();
				long elapsedTime = stopTime - startTime;

				if(validSequence)
				{				
					System.out.println("Next fibonacci number is " + nextFiboNumber);
					// print the fibonacci list
					System.out.println("Fibonacci list with next number");
					for(int i=0;i<fiboList.length;i++)
					{
						System.out.print(fiboList[i] + " ");
					}
					// print the last fibonacci number
					System.out.print(nextFiboNumber);
					System.out.println("\nExecution time - "+elapsedTime+" ms");
				}			
			}
		}
	}

	/**
	 * get the next fibonacci number for a given fibonacci sequence
	 * @param list fibonacci sequence
	 * @return next fibonacci number
	 */
	protected static long nextFibo(List<Long> list)
	{
		// get the last number and second last number in given fibonacci sequence
		long lastNumber = list.get(list.size()-1);
		long secondLastNumber = list.get(list.size()-2);
		// sum up the last two fibonacci numbers to get the next fibonacci number in the sequence
		long num = lastNumber + secondLastNumber;
		return num;
	}

	/**
	 * validate a given user input sequence to be a fibonacci sequence
	 * @param list user input sequence
	 * @param fiboList finbonacci sequence that is built to compare with user input
	 * @return true is valid fibonacci sequence
	 */
	protected static boolean validateFiboSequence(List<Long> list, long[] fiboList)
	{	
		// generate the fibonacci sequence
		for (int i=0;i<list.size();i++)
		{
			// generate the first two fibonacci elements
			if (i == 0 || i == 1)
			{
				fiboList[i] = 1;
			}
			else
			{
				fiboList[i] = fiboList[i-1] + fiboList[i-2];
			}
			// compare both the lists to validate
			if(fiboList[i]!=list.get(i))
			{
				System.out.println("Invalid fibonacci sequence");
				return false;
			}
		}
		return true;
	}
}
