import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Generate fibonacci set for a given sample size
 * @author Singaram
 */
public class FibonacciSet {

	public static void main(String[] args) throws NumberFormatException, IOException 
	{
		/* max limit that can execute in less than a second for iterative approach
		 * max limit for recursive approach is 40. iterative approach is used here as it is faster.
		 */
		int maxIterativeLimit = 170000000;
		int maxRecursiveLimit = 40;
		// get the input from user
		System.out.println("Enter the fibonacci size");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int limit = Integer.parseInt(br.readLine());
		
		if (limit > maxIterativeLimit)
		{
			// print an error on console as limit exceeded maximum
			System.out.println("Unable to calculate fibonacci sequence as size greater than max limit is entered");
			System.out.println("Max limit is 170000000");
		}
		else
		{
			// keep track of starting time
			long startTime = System.currentTimeMillis();
			
			long[] list = iterative(limit);
			
			// track the end time and calculate the execution time
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;

			// print the fibonacci list
			System.out.println("*******Iterative method*******");
			for (int i=0;i<limit;i++)
			{
				System.out.print(list[i] + " ");
			}
			System.out.println("\nIterative execution time - " + elapsedTime + " ms");
			
			/* recursive approach is implemented but not used as iterative is faster */
			if(limit > maxRecursiveLimit)
			{
				System.out.println("\nUnable to calculate recursively as max limit exceeded for recursive method");
				System.out.println("Max limit for recurive is 40");
			}
			else
			{
				recursive(limit);
			}
		}
	}
	
	/**
	 * Generate fibonacci sequence iteratively
	 * @param limit sample size for fibonacci sequence
	 * @return fibonacci sequence
	 */
	protected static long[] iterative(int limit)
	{
		// create a list for specified size
		long[] list = new long[limit];

		// generate the fibonacci sequence
		for (int i=0;i<limit;i++)
		{
			// generate the first two fibonacci elements
			if (i == 0 || i == 1)
			{
				list[i] = 1;
			}
			else
			{
				list[i] = list[i-1] + list[i-2];
			}
		}
				
		return list;
	}
	
	/**
	 * Recursive implementation of fibonacci set
	 */
	private static void recursive(int limit)
	{
		// create a list for specified size
		int[] fiboList = new int[limit];
		
		// keep track of starting time
		long startTime = System.currentTimeMillis();
		for (int i=0;i<limit;i++)
		{
			// generate each fibonacci number recursively
			fiboList[i] = fibonacciSeries(i);
		}
		
		// track the end time and calculate the execution time
		long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    
	    // print the fibonacci list
	    System.out.println("\n*******Recursive method*******");
		for (int i=0;i<limit;i++)
		{
			System.out.print(fiboList[i] + " ");
		}
		System.out.println("\nRecursive execution time - " + elapsedTime + " ms");
	}
	
	/**
	 * Calculate fibonacci sequence recursively
	 * @param num element to calculate fibonacci
	 * @return fibonacci number
	 */
	private static int fibonacciSeries (int num)
	{
		if (num == 0 || num == 1)
		{
			return 1;
		}
		return fibonacciSeries(num-1) + fibonacciSeries(num-2);		
	}
}
