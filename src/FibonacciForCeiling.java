import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Generate fibonacci sequence less than given ceiling value
 * @author Singaram
 */
public class FibonacciForCeiling {

	public static void main(String[] args) throws NumberFormatException, IOException 
	{
		// get the user input for ceiling value
		System.out.println("Enter the fibonacci ceiling value");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		long ceiling = Long.parseLong(br.readLine());
				
		// keep track of starting time
		long startTime = System.currentTimeMillis();

		// call fibonacci method passing ceiling value
		List<Long> list = fibonacciSet(ceiling);
		
		// track the end time and calculate the execution time
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		
		// print the fibonacci list
		for (int i=0;i<list.size();i++)
		{
			System.out.print(list.get(i) + " ");
		}		
		System.out.println("\nExecution time - " + elapsedTime + " ms");
	}
	
	/**
	 * generate fibonacci sequence for a given ceiling value
	 * @param ceiling ceiling value
	 * @return fibonacci sequence
	 */
	protected static List<Long> fibonacciSet(long ceiling)
	{
		// list is used here as the size of the fibonacci sequence is unknown
		List<Long> list = new ArrayList<Long>();
		
		// counter for fibonacci list index
		int ctr = 0; 
		// calculated fibonacci number
		long num = 0;
		
		// loop through while fibonacci number less than provided ceiling value 
		while (num < ceiling)
		{
			// first two fibonacci numbers
			if(ctr == 0 || ctr == 1)
			{
				list.add((long) 1);
				num = 1;
				ctr++;
			}
			else
			{
				// get the last two numbers in fibonacci list and sum up
				long i = (long) list.get(ctr-1);
				long j = (long) list.get(ctr-2);
				num = i + j;
				// if fibonacci number greater than ceiling value, break the loop, else add to list
				if (num >= ceiling)
					break;
				list.add(num);
				// increment counter
				ctr++;
			}
		}		
		return list;		
	}
}
